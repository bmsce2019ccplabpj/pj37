#include<stdio.h>
void swap(int *a, int *b)
{
    int t;
    t=*a;
    *a=*b;
    *b=t;
}
int main()
{
    int a,b;
    printf("Enter number a= ");
    scanf("%d",&a);
    printf("Enter number b= ");
    scanf("%d",&b);
    swap(&a,&b);
    printf("The numbers after swapping is a=%d and b=%d\n",a,b);
    return 0;
}