#include <stdio.h>
int main()
{
    int n,i,t;
    int small,large,small_pos,large_pos;
    printf("Enter the number of element in the array");
    scanf("%d",&n);
    int a[n];
    for(i=0;i<n;i++)
    {
        printf("Enter element of array:");
        scanf("%d",&a[i]);
    }
    printf("Elements of the array are:");
    for(i=0;i<n;i++)
    {
        printf("\t%d",a[i]);
    }
    small=a[0];
    small_pos=0;
    for(i=0;i<n;i++)
    {
        if(a[i]<small)
        {
            small=a[i];
            small_pos=i;
        }
    }
    large=a[0];
    large_pos=0;
    for(i=0;i<n;i++)
    {
        if(a[i]>large)
        {
            large=a[i];
            large_pos=i;
        }
    }
    printf("\nThe actual largest number is %d and its position is %d",large,large_pos);
    printf("\nThe actual smallest number is %d and its position is %d",small,small_pos);
    t=a[large_pos];
    a[large_pos]=a[small_pos];
    a[small_pos]=t;
    printf("\nthe largest number is:%d",a[large_pos]);
    printf("\nThe smallest number is:%d",a[small_pos]);
    printf("\nThe new array is:");
    for(i=0;i<n;i++)
    {
        printf("\t%d",a[i]);
    }
}